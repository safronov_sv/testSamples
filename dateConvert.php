<?php
class Avito
{

    /**
     * Convert current time specific date form avito.ru
     * like " сегодня 14:22" to unixtime format.
     *
     * @param $avitoDatetime
     *
     * @return string
     */
    private function avitoTimeToUnixtime($avitoDatetime)
    {
        $search = ['Сегодня', 'Вчера', 'января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'];
        $replace = ['today', 'yesterday', 'january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december'];
        $avitoDatetime = str_replace($search, $replace, $avitoDatetime);

        $avitoDatetime = str_replace(', ', ' ', $avitoDatetime);
        $avitoDatetime = trim($avitoDatetime);
        $avitoDatetimeArr = explode(' ', $avitoDatetime);

        if (count($avitoDatetimeArr) == 3) {
            $convertedDate = $avitoDatetimeArr[0] . ' ' . $avitoDatetimeArr[1];
            $date = DateTime::createFromFormat('U', strtotime($convertedDate));
            $hours = explode(':', $avitoDatetimeArr[2])[0];
            $minuts = explode(':', $avitoDatetimeArr[2])[1];
            $time = new DateInterval('PT' . $hours . 'H' . $minuts . 'M');
            $date->add($time);
        } else {
            $date = DateTime::createFromFormat('U', strtotime($avitoDatetime));
        }

        return $date->format('U');
    }

}
